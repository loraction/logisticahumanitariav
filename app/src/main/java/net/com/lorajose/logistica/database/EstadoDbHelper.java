package net.com.lorajose.logistica.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class EstadoDbHelper extends SQLiteOpenHelper {
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_CONTACTO = "CREATE TABLE " +
            DefinirTabla.Contacto.TABLE_NAME + " ("+
            DefinirTabla.Contacto._ID + " INTEGER PRIMARY KEY, "+
            DefinirTabla.Contacto.COLUMN_NAME_NOMBRE + TEXT_TYPE +
            COMMA_SEP +
            DefinirTabla.Contacto.COLUMN_NAME_STATUS + INTEGER_TYPE +
            ")";
    private static final String SQL_DELETE_CONTACTO = "DROP TABLE IF EXISTS " +DefinirTabla.Contacto.TABLE_NAME;
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "agenda.db";
    public EstadoDbHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int
            newVersion){
        db.execSQL(SQL_DELETE_CONTACTO);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int
            newVersion){
        onUpgrade(db, oldVersion, newVersion);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_CONTACTO);
    }
}
package net.com.lorajose.logistica;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import net.com.lorajose.logistica.database.DbEstados;
import net.com.lorajose.logistica.database.Estados;

public class MainActivity extends AppCompatActivity {
    private EditText edtNombre;
    private CheckBox chkStatus;
    private Estados savedContact;
    private int id;
    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;
    private int Contenedor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String[] estados = getResources().getStringArray(R.array.estados);

        edtNombre = (EditText) findViewById(R.id.edtNombre);
        chkStatus = (CheckBox) findViewById(R.id.cbxStatus);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        SharedPreferences prefs = getBaseContext().getSharedPreferences("vacio", Context.MODE_PRIVATE);
        Contenedor = prefs.getInt("id",0);
        if(Contenedor==0){
            SharedPreferences prefs1 = getSharedPreferences("vacio", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs1.edit();
            editor.putInt("id", Contenedor+1);
            editor.commit();
            for(int x = 0; x <10;x++){
                DbEstados source = new DbEstados(MainActivity.this);
                source.openDatabase();
                Estados nEstado = new Estados();
                nEstado.setNombre(estados[x]);
                nEstado.setStatus(1);
                source.insertEstado(nEstado);
                source.close();
            }
        }

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean completo = true;
                if (edtNombre.getText().toString().equals("")) {
                    edtNombre.setError("Introduce el nombre");
                    completo = false;
                }
                if (completo) {
                    DbEstados source = new DbEstados(MainActivity.this);
                    source.openDatabase();

                    Estados nEstado = new Estados();
                    nEstado.setNombre(edtNombre.getText().toString());
                    nEstado.setStatus(chkStatus.isChecked() ? 1 : 0);

                    if (savedContact == null) {
                        source.insertEstado(nEstado);
                        Toast.makeText(MainActivity.this,R.string.mensaje, Toast.LENGTH_SHORT).show();
                        limpiar();
                    } else {
                        source.updateEstado(nEstado,id);

                        Toast.makeText(MainActivity.this, R.string.mensajeedit,

                                Toast.LENGTH_SHORT).show();
                        limpiar();
                    }

                    source.close();

                }
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,
                        ListaActivity.class);
                startActivityForResult(i, 0);
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode,Intent data)
    {
        if (Activity.RESULT_OK == resultCode) {
            Estados contacto = (Estados) data.getSerializableExtra("estado");
            savedContact = contacto;
            id = contacto.get_ID();
            edtNombre.setText(contacto.getNombre());
            if (contacto.isStatus() > 0) {
                chkStatus.setChecked(true);
            }
        }else{
            limpiar();
        }
    }
    public void limpiar(){
        savedContact = null;
        edtNombre.setText("");
        chkStatus.setChecked(false);
    }
}
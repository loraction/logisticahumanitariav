package net.com.lorajose.logistica.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class DbEstados {
    Context context;
    EstadoDbHelper mDbHelper;
    SQLiteDatabase db;
    String[] columnsToRead = new String[]{
            DefinirTabla.Contacto._ID,
            DefinirTabla.Contacto.COLUMN_NAME_NOMBRE,
            DefinirTabla.Contacto.COLUMN_NAME_STATUS
    };

    public DbEstados(Context context) {
        this.context = context;
        mDbHelper = new EstadoDbHelper(this.context);
    }

    public void openDatabase() {
        db = mDbHelper.getWritableDatabase();
    }

    public long insertEstado(Estados c) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contacto.COLUMN_NAME_NOMBRE, c.getNombre());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_STATUS, c.isStatus());
        return db.insert(DefinirTabla.Contacto.TABLE_NAME, null, values);
//regresa el id insertado
    }

    public long updateEstado(Estados c, int id) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contacto.COLUMN_NAME_NOMBRE, c.getNombre());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_STATUS, c.isStatus());
        Log.d("values", values.toString()); //numero de filas afectadas
        return db.update(DefinirTabla.Contacto.TABLE_NAME, values,
                DefinirTabla.Contacto._ID + " = " + id, null);
    }

    public int deleteEstado(long id) {
        return db.delete(DefinirTabla.Contacto.TABLE_NAME, DefinirTabla.Contacto._ID + "=?",
                new String[]{String.valueOf(id)});
    }
    private Estados readEstado(Cursor cursor){
        Estados c = new Estados();
        c.set_ID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setStatus(cursor.getInt(2));
        return c;
    }
    public Estados getContacto(long id){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Contacto.TABLE_NAME,
                columnsToRead,

                DefinirTabla.Contacto._ID + " = ?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null
        );
        c.moveToFirst();
        Estados contacto = readEstado(c);
        c.close();
        return contacto;
    }
    public ArrayList<Estados> allEstados(){
        Cursor cursor = db.query(DefinirTabla.Contacto.TABLE_NAME,columnsToRead, null, null, null, null, null);
        ArrayList<Estados> contactos = new ArrayList<Estados>();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Estados c = readEstado(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }
    public void close(){
        mDbHelper.close();
    }
}
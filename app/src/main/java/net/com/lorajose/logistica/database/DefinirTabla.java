package net.com.lorajose.logistica.database;

import android.provider.BaseColumns;

public final class DefinirTabla {
    public DefinirTabla(){}
    public static abstract class Contacto implements BaseColumns {
        public static final String TABLE_NAME = "contactos";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        public static final String COLUMN_NAME_STATUS = "status";
    }
}